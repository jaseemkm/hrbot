from django.shortcuts import render
from .models import Leave
from datetime import date,timedelta

# Create your views here.

def apply_leave(user):
    leave = Leave()
    leave.user = user 
    leave.start_date = date.today() + timedelta(days=1)
    leave.save() 
    print("applied for leave")