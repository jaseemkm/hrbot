from django.db import models

# Create your models here.
class User(models.Model):
	slack_id = models.CharField(max_length=20, db_index=True)
	username = models.CharField(max_length=20, db_index=True)

class Leave(models.Model):
	user = models.ForeignKey(User,
							related_name="leave",
							on_delete=models.CASCADE)
	start_date = models.DateTimeField(null=True, blank=True, editable=False)
	end_date = models.DateTimeField(null=True, blank=True, editable=False)

	def save(self):
		leave_status = LeaveStatus.objects.filter(user=self.user).first()
		if leave_status:
			leave_status.availed_count = leave_status.availed_count + 1
			leave_status.save()
	
class LeaveStatus(models.Model):
	user = models.ForeignKey(User,
							related_name="leavestatus",
							on_delete=models.CASCADE)
	total_count = models.IntegerField(null=True, blank=True)
	availed_count = models.IntegerField(null=True, blank=True,default=0)


	