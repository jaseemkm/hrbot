from django.core.management.base import BaseCommand
from slackclient import SlackClient
import time
from user.models import User,Leave,LeaveStatus
import requests
from datetime import date,timedelta
from user.views import apply_leave


def start_listening():
    # Bot User OAuth Access Token
    token = 'xoxb-593680923729-588796865315-uxgs67M7QB23Jybbtl1s8UFD'
    slackClient = SlackClient(token)
    # Reading messages from slack
    if slackClient.rtm_connect():
        while True:
            events = slackClient.rtm_read()
            for event in events:
                if event['type'] == 'message' and "subtype" not in event:
                    print (event)
                    if event['user']:
                        slack_id = event['user']
                        try:
                            user = User.objects.get(slack_id=slack_id)
                        except User.DoesNotExist:
                            request_link = 'https://slack.com/api/users.info?token=xoxb-593680923729-588796865315-uxgs67M7QB23Jybbtl1s8UFD&user=' + slack_id
                            print(request_link)
                            user_details = requests.get(request_link)
                            user_details = user_details.json()
                            username = user_details['user']['profile']['real_name']
                            user = User(username=username, slack_id=slack_id)
                            user.save()
                            leave_status = LeaveStatus()
                            leave_status.user = user
                            leave_status.total_count = 12
                            leave_status.save()
                    if event['text'] == "LEV":
                        apply_leave(user)
                        text = "Leave applied  :tada:" + user.username
                        slackClient.api_call(
                            "chat.postMessage",
                            channel="CHRFADA07",
                            text=text,
                            thread_ts=event['ts'],
                            reply_broadcast=True)
                    if event['text'] == "STATUS":
                        leave_status = LeaveStatus.objects.filter(user=user).first()
                        if leave_status:
                            balance_leave = leave_status.total_count - leave_status.availed_count
                            text = "You have " + str(balance_leave) + "out of " + str(leave_status.total_count)
                            slackClient.api_call(
                                "chat.postMessage",
                                channel="CHRFADA07",
                                text=text,
                                thread_ts=event['ts'],
                                reply_broadcast=True)
            time.sleep(1)


class Command(BaseCommand):
    def handle(self, **option):
        session = start_listening()
